* Clone with `--recursive` - for getting all submodules (plugins in `/bundle`)

* For linting of API Blueprint - install parser [drafter](https://github.com/apiaryio/drafter) - needed Syntastic already in /bundle
brew install --HEAD https://raw.github.com/apiaryio/drafter/master/tools/homebrew/drafter.rb

For validation of js by Syntastic:
`npm install -g eshint`
https://www.sitepoint.com/comparison-javascript-linting-tools/

Install tern:
cd bundle/tern_for_vim
npm install

For validation html by the Syntastic:
`brew install tidy-html5`

For Python3 for Syntastic:
pip3 install pylink, flake8, mypy
If permission denied on install: add --user

## How to add new plugin:  
cd pack/git-submodules-plugins/start
git submodule add http://github.com/davidhalter/jedi-vim.git  
Possible to execute for plugin that already exists in bundle/

For correct removing of the submodule:
git rm <name>
