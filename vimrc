" Set the title for the current tab - name and path to the current file
set title

" Do not create .swp.* files
" against committing this .swp, adding them to archive, grep, ...
" also for faster Vim.
set noswapfile
" https://stackoverflow.com/a/821936/1879101

au BufReadPost *
            \ if line("'\"") > 0 && line("'\"") <= line("$") |
                \ execute("normal `\"") |
            \ endif
" https://vi.stackexchange.com/questions/106/how-can-i-restore-the-cursor-position-when-opening-a-file

autocmd FileType gitcommit call setpos('.', [0, 1, 1, 0])
 " https://stackoverflow.com/questions/16727923/how-to-modify-the-last-position-jump-vimscript-to-not-do-it-for-git-commit-messa

autocmd FileType gitcommit,markdown setlocal wrap linebreak spell
" https://stackoverflow.com/questions/1691060/vim-set-spell-in-file-git-commit-editmsg

" TODO autoreload buffer on focus, commented because not tested,
" I need this for CSS.
" autocmd FileType css set autoread

syntax on

let b:ale_linters = {'python': ['pylint', 'flake8', 'mypy']}
let g:ale_set_balloons = 1

" Do not use tidy-html for hugo-html because of false positive
" https://github.com/w0rp/ale/issues/371
let g:ale_pattern_options = {
\    '/Users/vitaly/intelligent-speaker/website/layouts/.*\.html$': {'ale_enabled': 0}
\}

let g:ale_echo_msg_format = '[%linter%] %[code]% %s [%severity%]'
let g:ale_python_flake8_options = '--max-line-length=100'
let g:ale_lint_delay = 999
" Delay against fan noise from MacBook
" http://pep8.readthedocs.io/en/release-1.7.x/intro.html#configuration
" https://github.com/w0rp/ale
" pip3 install pylint flake8 mypy --user
" npm install eslint --global
let g:ale_lint_on_enter = 0  " Against slow switching between buffers
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
" https://github.com/w0rp/ale#5ix-how-can-i-navigate-between-errors-quickly

" https://github.com/vim-python/python-syntax#configuration
let g:python_highlight_all = 1
" This enables all highlights from improved python highlighter plugin
" I need it for template literals

filetype plugin indent on

" set ts=4 " tab size
 set sw=4
 " shift width - size of manual mass indent by > or < on selected

set expandtab ts=4
" when edit *.apib (this is apiblueprint mson for apiary.io) - tab insert four spaces
" instead of illegal tab symbol
" autocmd FileType apiblueprint set expandtab ts=4
" commented because now this is default
" http://stackoverflow.com/questions/158968/changing-vim-indentation-behavior-by-file-type
autocmd Filetype go setlocal noexpandtab  " go fmt anyway replace spaces to tabs
" https://stackoverflow.com/questions/158968/changing-vim-indentation-behavior-by-file-type

set backspace=indent,eol,start
" With this backspace works and I can remove to the upper lines too
" https://stackoverflow.com/questions/11560201/backspace-key-not-working-in-vim-vi

set listchars=eol:$,tab:>-,extends:>,precedes:< " Show non-printable characters
set list " Shows tabs and end of line
" http://vim.wikia.com/wiki/Highlight_unwanted_spaces
" for another non-keyboard chars https://unicode-table.com
" but not all terms can display all of unicode,
" also over ssh you can see not all chars

set cursorline " underline line with cursor

" blue, enabled because in term linux (tty) cursorline does not works
" highlight CursorLineNr ctermfg=4
" commented because looks like ugly blue in Term.app

" Copy unnamed ("default") register to the system clipboard.
" I have this command because `apt install vim` on Ubuntu
" install version with '-clipboard', without access to the system clipboard.
" I need this command to copy selected -
" I tried to send selected to external command but Vim send entire line, see
" https://stackoverflow.com/a/40073358/1879101
command Copy call system('xsel --clipboard --input', @") | echo 'Copied'

if system('uname -s') == "Darwin\n"
    " `if has('mac')` wors only for Vim from Homebrew - not for preinstalled.

    " Theme https://github.com/tomasr/molokai used not as a candy but because
    " default theme with js syntax plugin highlight in the same color strings and
    " the names of reserved variables like `console` or `Number`, see my issue at
    " https://github.com/pangloss/vim-javascript/issues/1019
    " Also this theme alter horizontal highlighter of the current line
    " from underline to background to the full height of the line.
    " colorscheme molokai" in directory `colors`
    "let g:molokai_original = 1
    colorscheme gotham256 " better for default term app in macOS when Molokai is better for iTerm2

    highlight CursorColumn ctermbg=17
    " looks like almost transparent darkblue
    " this is darker visible that can be
    " check for all possible colors here
    " https://github.com/guns/xterm-color-table.vim
    " TODO copy code from link to this file
        " for checking colors on different envs -
        " because I found that for example the same color value
        " looks different on tty and in xterm, also tty
        " have less colors
else
    if &term == 'linux'
        " tty on Ctrl+Alt+F1,
        " Terminator on X returns "xterm"
        highlight CursorColumn ctermbg=5    " purple
    else  " Xorg terminal
        highlight CursorColumn ctermbg=black
        highlight Normal ctermbg=None
    endif
endif
set cursorcolumn

highlight CursorLine ctermbg=17 ctermfg=white

" search and highlight words under cursor
autocmd CursorMoved * exe printf('match IncSearch /\V\<%s\>/', escape(expand('<cword>'), '/\'))

" go to opening or closing of parent tag in xml
nnoremap ]t vatatv
nnoremap [t vatatov
" https://stackoverflow.com/questions/6169537/vim-movements-going-to-parent

" Switch to last-active tab.
" From
" https://stackoverflow.com/questions/2119754/switch-to-last-active-tab-in-vim
" Use
" \`
" \ is a default <leader>
if !exists('g:Lasttab')
    let g:Lasttab = 1
    let g:Lasttab_backup = 1
endif
autocmd! TabLeave * let g:Lasttab_backup = g:Lasttab | let g:Lasttab = tabpagenr()
autocmd! TabClosed * let g:Lasttab = g:Lasttab_backup
nmap <silent> <Leader>` :exe "tabn " . g:Lasttab<cr>

" activate build-in plugin:
" use % to go from opening xml tag to closing
" and vice versa
" - like moving from { to } in default preferences
runtime macros/matchit.vim

" search selected by //
" http://vim.wikia.com/wiki/Search_for_visually_selected_text
vnoremap // y/<C-R>"<CR>

" highlight search
set hlsearch

" incremental search - search on every char
set incsearch

" increase limit of copied (yanked) lines:
" '100 marks will be remembered for the last 100 edited files;
" (this can not be deleted though I do not need it)
" maximum 1000 lines instead of default 50;
" maximum 20 kilobytes;
" http://vim.wikia.com/wiki/Copy,_cut_and_paste
set viminfo='100,<1000,s20

" enable mouse click to move caret and select with mouse
set mouse=a
" https://superuser.com/questions/146768/mouse-cursor-in-terminal

" activate setting of prefs per file in comments
set modeline
" http://stackoverflow.com/questions/313463
"
set modelines=1
" Without that no effect on Mac with for example such comment in HTML:
" <!-- vim: set filetype=text: -->
" https://unix.stackexchange.com/a/20083/34318
" https://security.stackexchange.com/questions/36001/vim-modeline-vulnerabilities

" color of the delimiter of two opened windows
" ctermbg setted to have invisible ascii in xterm
highlight VertSplit ctermbg=black ctermfg=black

" vim-signature: sync color with git-gutter 
let g:SignatureMarkTextHLDynamic = 1

" Context ("unfilied" in Git terminology) of git difftool
:set diffopt=filler,context:9

" Against very slow Vim when big oneline.
" https://stackoverflow.com/questions/901313
set nowrap

" Syntax highlight for max line width - for speed.
" By default Vim will highlight full line - even if this line is very long
" and you will need to wait before Vim unfreeze.
set synmaxcol=300

" This enable autocompletion (Ctrl + X P) for strings like aaa-bbb-ccc,
" without that will suggest only aaa for a.
" https://stackoverflow.com/questions/10789430/vims-ctrlp-autocomplete-for-identifiers-with-dash
set iskeyword+=\-

set laststatus=0 " Do not show statusline
highlight StatusLine ctermbg=DarkGreen ctermfg=black  " Windows splitted by status line
highlight StatusLineNC ctermbg=DarkMagenta ctermfg=black  " NC mean Not Current


" print the name of the current function in command line by Ctrl + g f
nnoremap <C-g>f :echo cfi#format("%s", "")<CR>

" Go to definition by F5
" https://vi.stackexchange.com/a/4976/6038
nnoremap <F5> :grep! "function \<<cword>\>" . -r<CR>:cc<CR>

" In command line at right - show the amount of selected chars or lines (in Visual mode)
set showcmd

" Bottom right: do not show vertical/horizontal position numbers
set noruler

set encoding=utf-8


" ===== START vim-force.com =====
" see all available options here
" https://github.com/neowit/vim-force.com/blob/master/doc/force.com.txt
" required options
" let g:apex_tooling_force_dot_com_path = "/Users/admin/.vim/vim-force-settings/tooling-force.com-0.3.8.0.jar"
" let g:apex_backup_folder = "/Users/admin/.vim/vim-force-settings/apex_backup_folder"
" let g:apex_temp_folder = "/Users/admin/.vim/vim-force-settings/apex_temp_folder"
" let g:apex_properties_folder = "/Users/admin/.vim/vim-force-settings/apex_properties_folder"

" split screen for messages hirozontaly - for portrait display position
" let g:apex_messages_split_type = 'split'
" defeult value is `vsplit` - for display in usual landscape position

" optional options
" let g:apex_server_timeoutSec = 200

" F2 will deploy current file to Salesforce without confirmation
" nmap <F2> :w<CR>:ApexSaveOne<CR>y<CR>
" ===== END vim-force.com =====

if exists('$TMUX')
        " tmux: show title of the document in tabs"
        autocmd BufReadPost,FileReadPost,BufNewFile * call system("tmux rename-window " . expand("%:t"))
        autocmd VimLeave * call system("tmux setw automatic-rename")
        " https://stackoverflow.com/questions/15123477/tmux-tabs-with-name-of-file-open-in-vim
endif

autocmd BufNewFile,BufRead /home/vitaly/global-marketing-solutions/marketegy/frontend/src/* set syntax=

" Remain undo/redo when switching between buffers
set hidden

" History of commands (:) and search (/); you see history at :his or q:, good for completion
set history=2000
